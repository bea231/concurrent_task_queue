#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <chrono>
#include <future>

#include <queue/CoarseSyncTaskQueue.h>
#include <tasks/EmptyTask.h>
#include <tasks/PrintFileDescriptorTask.h>

using namespace std::chrono_literals;

namespace {
    TEST(PrintFileDescriptorTest, EmptyString) {
        testing::internal::CaptureStdout();
        std::invoke(PrintFileDescriptorTask(""));
        auto stdout_str = testing::internal::GetCapturedStdout();

        EXPECT_THAT(stdout_str, testing::StartsWith(PrintFileDescriptorTask::ERROR_PREFIX));
    }

    TEST(PrintFileDescriptorTest, NonExistentFile) {
        testing::internal::CaptureStdout();
        // The following symbols are forbidden in filenames in most of the OS
        std::invoke(PrintFileDescriptorTask("~@#$%^-_(){}"));
        auto stdout_str = testing::internal::GetCapturedStdout();

        EXPECT_THAT(stdout_str, testing::StartsWith(PrintFileDescriptorTask::ERROR_PREFIX));
    }

    TEST(PrintFileDescriptorTest, ExistingFile) {
        testing::internal::CaptureStdout();
        // This test assumes, that __FILE__ exists during tests run
        std::invoke(PrintFileDescriptorTask(__FILE__));
        auto stdout_str = testing::internal::GetCapturedStdout();

        std::ostringstream descriptorRegExStream;
        descriptorRegExStream << PrintFileDescriptorTask::DESCRIPTOR_PREFIX << "\\d";
        EXPECT_THAT(stdout_str, testing::ContainsRegex("File opening success. fd=[0-9]+\n"));
    }

    TEST(ProducerTest, Nothing) {
        CoarseSyncTaskQueue q;
        EXPECT_EQ(q.size_guess(), 0);
    }

    TEST(ProducerTest, FewPushes) {
        constexpr unsigned int tasksCnt = 5;
        CoarseSyncTaskQueue q;
        for (unsigned int i = 0; i < tasksCnt; ++i) {
            q.push(EmptyTask());
        }
        // Since worker thread is not running by default all pushed tasks should be in queue
        EXPECT_EQ(q.size_guess(), tasksCnt);
    }

    TEST(ConsumerTest, EmptyIdempotence) {
        CoarseSyncTaskQueue q;
        q.start_tasks_processing();
        q.start_tasks_processing();
        q.wait_all_and_stop();
        q.wait_all_and_stop();
    }

    TEST(ConsumerTest, FilledIdempotence) {
        CoarseSyncTaskQueue q;
        constexpr unsigned int tasksCnt = 20;

        EXPECT_EQ(q.size_guess(), 0);
        for (unsigned int i = 0; i < tasksCnt; ++i)
            q.push(EmptyTask());
        EXPECT_EQ(q.size_guess(), tasksCnt);
        q.start_tasks_processing();
        q.start_tasks_processing();

        q.wait_all_and_stop();
        EXPECT_EQ(q.size_guess(), 0);
        q.wait_all_and_stop();
        EXPECT_EQ(q.size_guess(), 0);
    }

    TEST(ConsumerTest, WrongOrder) {
        CoarseSyncTaskQueue q;
        q.push(EmptyTask());

        EXPECT_EQ(q.size_guess(), 1);
        q.wait_all_and_stop();
        EXPECT_EQ(q.size_guess(), 1);
        q.start_tasks_processing();
        q.wait_all_and_stop();
        EXPECT_EQ(q.size_guess(), 0);
    }

    TEST(ConsumerTest, Restart) {
        CoarseSyncTaskQueue q;
        size_t restartCnt = 1;
        while (restartCnt-- > 0) {
            q.push(EmptyTask());
            EXPECT_EQ(q.size_guess(), 1);
            q.start_tasks_processing();
            q.wait_all_and_stop();
            EXPECT_EQ(q.size_guess(), 0);
        }
    }

    TEST(ProducerConsumerTest, CheckFutureStates) {
        constexpr int expectedReturn = 5;
        std::packaged_task<int()> task([]() { return expectedReturn; });
        auto task_future = task.get_future();

        CoarseSyncTaskQueue q;
        q.push([&task]() {
            task();
        });
        EXPECT_EQ(task_future.wait_for(1ms), std::future_status::timeout);
        EXPECT_EQ(q.size_guess(), 1);

        q.start_tasks_processing();
        EXPECT_EQ(task_future.get(), expectedReturn);
        EXPECT_EQ(q.size_guess(), 0);
    }
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}