#include <iostream>

#include "queue/CoarseSyncTaskQueue.h"
#include "tasks/PrintFileDescriptorTask.h"

int main() {
    using namespace std::chrono_literals;
    CoarseSyncTaskQueue q;
    q.start_tasks_processing();

    q.push(PrintFileDescriptorTask("../src/main.cpp"));
    q.push(PrintFileDescriptorTask("some_not_existent_file"));

    return 0;
}
