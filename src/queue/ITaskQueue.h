#ifndef _ITASKQUEUE_H_
#define _ITASKQUEUE_H_

class ITaskQueue {
public:
    typedef std::function<void()> Task;
public:
    virtual ~ITaskQueue() = default;

    virtual void push(Task f) = 0;
    [[nodiscard]] virtual size_t size_guess() const  = 0;
    // Start tasks processing
    virtual void start_tasks_processing() = 0;
    // Wait for all jobs to be done and stop tasks processing
    virtual void wait_all_and_stop() = 0;
};

#endif //_ITASKQUEUE_H_
