#ifndef _COARSESYNCTASKQUEUE_H_
#define _COARSESYNCTASKQUEUE_H_

#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <queue>
#include <thread>
#include <type_traits>

#include "ITaskQueue.h"

class CoarseSyncTaskQueue final : public ITaskQueue {
private:
    std::queue<Task> task_queue_;

    std::thread worker_thread_;
    mutable std::mutex queue_mutex_;
    std::condition_variable has_task_condition_;
    std::atomic_bool should_stop_ = false;

public:
    CoarseSyncTaskQueue() = default;
    CoarseSyncTaskQueue(const CoarseSyncTaskQueue& other) = delete;
    CoarseSyncTaskQueue& operator=(const CoarseSyncTaskQueue& other) = delete;
    ~CoarseSyncTaskQueue() override {
        wait_all_and_stop();
    }

    void start_tasks_processing() override {
        if (!worker_thread_.joinable()) {
            worker_thread_ = std::thread(&CoarseSyncTaskQueue::worker, this);
        }
    }

    void wait_all_and_stop() override {
        if (!should_stop_.exchange(true)) {
            has_task_condition_.notify_all();
        }

        if (worker_thread_.joinable()) {
            worker_thread_.join();
        }
    }

    void push(Task f) override {
        static_assert(noexcept(f), "task should be noexcept");

        std::unique_lock<std::mutex> lock(queue_mutex_);
        try {
            task_queue_.push(std::move(f));
        } catch (const std::exception& e) {
            std::cout << "Cannot push task to queue: " << e.what() << std::endl;
        }
        lock.unlock();

        has_task_condition_.notify_one();
    }

    size_t size_guess() const override {
        std::unique_lock<std::mutex> lock(queue_mutex_);
        return task_queue_.size();
    }

private:
    void worker() {
        while (true) {
            std::unique_lock<std::mutex> lock(queue_mutex_);
            has_task_condition_.wait(lock, [this] {
                return !task_queue_.empty() || should_stop_.load();
            });

            if (should_stop_.load() && task_queue_.empty()) {
                return;
            }

            auto task = std::move(task_queue_.front());
            task_queue_.pop();
            lock.unlock();
            try {
                task();
            } catch (const std::exception &e){
                // Catching task exception in worker thread is kinda shifting of responsibility
                // Option1: require noexcept from tasks
                // Option2: use std::package_task
                std::cout << "Error in worker thread: " << e.what() << std::endl;
            }
        }
    }
};

#endif /* _COARSESYNCTASKQUEUE_H_ */
