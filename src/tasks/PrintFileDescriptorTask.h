#ifndef _PRINTFILEDESCRIPTORTASK_H_
#define _PRINTFILEDESCRIPTORTASK_H_

#include <cstring>
#include <cerrno>

#include <fcntl.h>
#include <unistd.h>

#include "ITask.h"

class PrintFileDescriptorTask final : public ITask {
private:
    std::string file_path_;
public:
    static constexpr auto ERROR_PREFIX = "File opening error: ";
    static constexpr auto DESCRIPTOR_PREFIX = "File opening success. fd=";

    explicit PrintFileDescriptorTask(const std::string_view& filePath) : file_path_(filePath) { }

    void operator()() override {
        int fd = open(file_path_.c_str(), O_RDONLY);
        if (fd >= 0) {
            std::cout << DESCRIPTOR_PREFIX << fd << std::endl;
            close(fd);
        } else {
            std::cout << ERROR_PREFIX << std::strerror(errno) << std::endl;
        }
    }
};

#endif //_PRINTFILEDESCRIPTORTASK_H_
