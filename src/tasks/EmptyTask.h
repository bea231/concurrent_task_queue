#ifndef _EMPTYTASK_H_
#define _EMPTYTASK_H_

#include "ITask.h"

class EmptyTask : public ITask {
public:
    void operator()() override {}
};

#endif //_EMPTYTASK_H_
