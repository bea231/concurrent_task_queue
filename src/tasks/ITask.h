#ifndef _ITASK_H_
#define _ITASK_H_

class ITask {
public:
    virtual ~ITask() = default;

    virtual void operator()() = 0;
};

#endif //_ITASK_H_
